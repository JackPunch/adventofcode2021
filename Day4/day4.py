import math
import operator
from functools import reduce
from typing import List, Tuple

test_input = """7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7"""


def prod(iterable):
    return reduce(operator.mul, iterable, 1)


def read_input(path: str, lines: bool = True):
    with open(path, "r") as f:
        input = f.readlines() if lines else f.read()
    return input


class BingoBoard:
    def __init__(self, numbers: List[int], row_count: int):
        self.numbers = numbers
        self.checked_numbers = []
        self.row_count = row_count
        self.blank_rows = [[0] * row_count for i in range(row_count)]
        self.last_number = None
        self.complete = False

    def check_bingo(self) -> bool:  # I'm just going to hard code this to a 5 x 5
        left_diagonal = 0
        right_diagonal = 0
        for i, row in enumerate(self.blank_rows):
            column_sum = 0
            left_diagonal += self.blank_rows[i][i]
            right_diagonal += self.blank_rows[~i][i]
            if sum(row) == 5:
                self.complete = True
                return True

            for j in self.blank_rows:
                column_sum += j[i]
            if column_sum == 5:
                self.complete = True
                return True
        if left_diagonal == 5 or right_diagonal == 5:
            self.complete = True
            return True

        return False

        # check diagonals

    def check_number_and_complete(self, number: int) -> bool:
        self.last_number = number
        if number in self.numbers:
            self.checked_numbers.append(number)
            index = self.numbers.index(number)
            row, column = self.get_coord(index)
            self.blank_rows[row][column] = 1

            return self.check_bingo()
        return False

    def get_coord(self, index: int):
        return (math.floor(index / self.row_count), index % 5)

    def get_result(self):
        sum_unmarked = sum([x for x in self.numbers if x not in self.checked_numbers])
        return sum_unmarked * self.last_number


def parse_input(input: str) -> Tuple[List[int], List[BingoBoard]]:
    on_blank_line = input.split("\n\n")
    bingo_call = [int(x) for x in on_blank_line[0].split(",")]
    parsed_boards = [list(map(lambda x: int(x), x.split())) for x in on_blank_line[1:]]
    bingo_boards = [BingoBoard(x, 5) for x in parsed_boards]
    return (bingo_call, bingo_boards)


def part1(bingo_calls: List[int], bingo_boards: List[BingoBoard]) -> int:
    for number in bingo_calls:
        for board in bingo_boards:
            if board.check_number_and_complete(number):
                return board.get_result()

    return -1


def part2(bingo_calls: List[int], bingo_boards: List[BingoBoard]) -> int:
    remove_boards = []
    for number in bingo_calls:
        for board in bingo_boards:
            if board.check_number_and_complete(number) and len(bingo_boards) > 1:
                remove_boards.append(board)
                # bingo_boards.remove(board)
            elif board.complete:
                return board.get_result()

        for i in remove_boards:
            bingo_boards.remove(i)
        remove_boards.clear()

    return -1


if __name__ == "__main__":
    input = read_input("Day4/input.txt", lines=False)

    parsed = parse_input(input)
    test_parsed = parse_input(test_input)

    print(part1(*parsed))
    print(part2(*parsed))
    print(part2(*test_parsed))

    print("Breakpoint")
