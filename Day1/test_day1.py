import pytest
from day1 import get_input, parse_input, part1, part2


actual_input = get_input("Day1/input.txt", lines=True)
parsed_input = parse_input(actual_input)

input1 = """
199
200
208
210
200
207
240
269
260
263
"""


@pytest.mark.parametrize(
    "test_input, output",
    [
        (
            [
                input1,
                ["199", "200", "208", "210", "200", "207", "240", "269", "260", "263"],
            ]
        )
    ],
)
def test_file_reader(test_input, output):
    pass


@pytest.mark.parametrize(
    "test_input, output",
    [
        (
            ["199", "200", "208", "210", "200", "207", "240", "269", "260", "263"],
            [199, 200, 208, 210, 200, 207, 240, 269, 260, 263],
        )
    ],
)
def test_parser(test_input, output):
    assert parse_input(test_input) == output


@pytest.mark.parametrize(
    "test_input, output", [([199, 200, 208, 210, 200, 207, 240, 269, 260, 263], 7), (parsed_input, 1228)]
)
def test_part1(test_input, output):
    rv = part1(test_input)
    assert rv == output


@pytest.mark.parametrize(
    "test_input, output", [([199, 200, 208, 210, 200, 207, 240, 269, 260, 263], 5), (parsed_input, 1257)]
)
def test_part2(test_input, output):
    rv = part2(test_input)
    assert rv == output
