from typing import Union, List


def get_input(path: str, lines: bool = False) -> Union[List[str], str]:
    with open(path, "r") as f:
        rv = f.readlines() if lines else f.read()
    return rv


def parse_input(str_list: List[str]) -> List[int]:
    return [int(x) for x in str_list]


def part1(input: List[int]) -> int:
    return len(
        list(
            filter(
                lambda i_v: i_v[1] < input[i_v[0] + 1] if i_v[0] < len(input) - 1 else False, enumerate(input)
            )
        )
    )


def part2(input: List[int]) -> int:
    return len(
        list(
            filter(
                lambda i_v: i_v[1] < input[i_v[0] + 3] if i_v[0] < len(input) - 3 else False, enumerate(input)
            )
        )
    )
