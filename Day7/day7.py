from typing import List


def read_input(path: str, lines: bool = True):
    with open(path, "r") as f:
        input = f.readlines() if lines else f.read()
    return input


def parse_input(input: str) -> List[int]:
    input_list = [int(x) for x in input.split(",")]
    return input_list


def part1(inputt: List[int]) -> int:
    med = sorted(inputt)[len(inputt) // 2]
    total_fuel = sum([abs(x - med) for x in inputt])
    return total_fuel


def part2(inputt: List[int]) -> int:
    """Tried mean initally with round and it's incorrect, applying both and checking min"""
    sorty = sorted(inputt)
    mean = sum(sorty) / len(sorty)
    usages = []
    for position in [int(mean // 1), round(mean)]:
        usages.append(sum([sum(range(abs(x - position) + 1)) for x in sorty]))
    total_fuel = min(usages)
    return total_fuel

    # Brute force after my initial mean assumption failed
    # positions = range(max(inputt) + 1)
    # for position in positions:
    #     usages.append(sum([sum(range(abs(x - position) + 1)) for x in sorty]))
    # total_fuel = min(usages)
    # print(usages.index(total_fuel))
    # return total_fuel


if __name__ == "__main__":

    inputt = read_input("Day7/input.txt", lines=False)

    parsed = parse_input(inputt)

    print(part1(parsed))

    print(part2(parsed))
