import pytest
from day7 import read_input, parse_input, part1, part2


TEST_INPUT = """16,1,2,0,4,2,7,1,2,14"""
PARSE_TEST_INPUT = parse_input(TEST_INPUT)


@pytest.mark.parametrize("input, output", [(PARSE_TEST_INPUT, 37)])
def test_part1(input, output):
    return part1(input) == output


@pytest.mark.parametrize("input, output", [(PARSE_TEST_INPUT, 168)])
def test_part2(input, output):
    return part2(input) == output
