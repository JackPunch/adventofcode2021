from typing import List, Set, Dict
from enum import Enum


def read_input(path: str, lines: bool = True):
    with open(path, "r") as f:
        input = f.readlines() if lines else f.read()
    return input


def parse_input(input: List[str]) -> List[int]:
    input_list = [int(x) for x in input.split(",")]
    return input_list


def part1(inputt: List[str]) -> int:
    count = 0
    UNIQUE_ARG_LENS = [2, 3, 4, 7]

    for line in inputt:
        left, right = line.split("|")
        right_args = right.strip().split()
        for arg in right_args:
            if len(arg) in UNIQUE_ARG_LENS:
                count += 1

    return count


class Segment(Enum):
    Top = 1
    Top_Left = 2
    Top_Right = 3
    Middle = 4
    Bottom_Left = 5
    Bottom_Right = 6
    Bottom = 7


class InputLine:
    def __init__(self, left_side: List[str], right_side: List[str]):
        self.known_numbers: Dict[int, Set[str[1]] | None] = {x: None for x in range(10)}
        self.known_segments: Dict[Segment, str[1] | None] = {}
        self.left_side: List[Set[str[1]]] = parse_input(left_side)
        self.right_side: List[Set[str[1]]] = parse_input(right_side)
        self.get_unique()

    @staticmethod
    def parse_to_set(side_of_input: str) -> List[Set[str[1]]]:
        return [set(x) for x in side_of_input.split()]

    def get_unique(self):
        """This finds the numbers 1, 4, 7, 8"""
        to_remove = []
        for n in self.left_side:
            count = len(n)
            # 1
            if count == 2:
                self.known_numbers[1] = n
                to_remove.append(n)
            # 7
            elif count == 3:
                self.known_numbers[7] = n
                to_remove.append(n)
            # 4
            elif count == 4:
                self.known_numbers[4] = n
                to_remove.append(n)
            # 8
            elif count == 7:
                self.known_numbers[8] = n
                to_remove.append(n)
        # Remove values we've found
        for n in to_remove:
            self.left_side.remove(n)

    def step1(self):
        # Get top segment
        self.known_segments[Segment.Top] = self.known_numbers[7].diffence(self.known_numbers[1])

        # Get 9
        for x in self.left_side:
            if len(x) == 6 and len(self.known_numbers[7].intersection(x)) == 4:
                self.known_numbers[9] = x
        self.left_side.remove(self.known_numbers[9])

        # Bottom left segment
        self.known_segments[Segment.Bottom_Left] = self.known_numbers[8].difference(self.known_numbers[9])

        # Get 2
        for x in self.left_side:
            if len(x) == 5 and self.known_segments[Segment.Bottom_Left] in x:
                self.known_numbers[2] = x
        self.left_side.remove(self.known_numbers[2])

        # Get bottom right segment
        self.known_segments[Segment.Bottom_Right] = self.known_numbers[1].difference(self.known_numbers[2])

        # Get 0
        for x in self.left_side:
            if len(x) == 6 and len(x.difference(self.known_numbers[1]) == 4):
                self.known_numbers[0] = x
        self.left_side.remove(self.known_numbers[0])

        # Get top left segment
        self.known_segments[Segment.Top_Left] = self.known_numbers[0].difference(
            self.known_numbers[2].difference(self.known_segments[Segment().Bottom_Right])
        )

        # Get 3
        self.known_numbers[3] = (
            self.known_numbers[2]
            - self.known_segments[Segment.Bottom_Left]
            + self.known_segments[Segment.Bottom_Right]
        )

        # Get top right
        self.known_segments[Segment.Top_Right] = self.known_numbers[5].difference(self.known_numbers[4])

        # Get middle segment
        self.known_numbers[8].difference(self.known_numbers[0])

        print(self.known_numbers)


if __name__ == "__main__":
    inputt = read_input("Day8/input.txt")
    print(part1(inputt))
