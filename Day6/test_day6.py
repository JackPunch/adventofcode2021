import pytest
from day6 import read_input, parse_input, part1

INPUT = read_input("Day6/input.txt", lines=False)
TEST_INPUT = """3,4,3,1,2"""

PARSED_TEST_INPUT = parse_input(TEST_INPUT)
PARSED_INPUT = parse_input(INPUT)


@pytest.mark.parametrize("input, output", [(PARSED_TEST_INPUT, 5934), (PARSED_INPUT, 380243)])
def test_part1(input, output):
    return part1(input, 80) == output


@pytest.mark.parametrize("input, output", [(PARSED_TEST_INPUT, 26984457539), (PARSED_INPUT, 1708791884591)])
def test_part2(input, output):
    return part1(input, 256) == output
