# parse into a dict of keys 0-7 or 1-8 and then add the amount at 1 to and 6 each turn, repeat 80 times...
# This assumes order doesn't matter
from collections import deque, Counter


def read_input(path: str, lines: bool = True):
    with open(path, "r") as f:
        input = f.readlines() if lines else f.read()
    return input


def parse_input(input: str) -> dict[int, int]:
    input_list = [int(x) for x in input.split(",")]
    return Counter(input_list)


def part1(input: dict[int, int], interations: int) -> int:
    old_dict = input
    for i in range(interations):
        new_dict = {}
        for key, value in old_dict.items():
            if key == 0:
                if new_dict.get(6, None) is not None:
                    new_dict[6] += old_dict[key]
                else:
                    new_dict[6] = old_dict[key]
                new_dict[8] = old_dict[key]
            else:
                if new_dict.get(key - 1, None) is not None:
                    new_dict[key - 1] += old_dict[key]
                else:
                    new_dict[key - 1] = old_dict[key]

        old_dict = new_dict

    return sum([x for x in old_dict.values()])


if __name__ == "__main__":
    input = read_input("Day6/input.txt", lines=False)
    parsed = parse_input(input)
    print(part1(parsed, 80))
    print(part1(parsed, 256))
