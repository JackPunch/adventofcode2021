import pytest
from day5 import part1, part2, read_input, parse_input

inputt = read_input("Day5/input.txt")
test_input = """0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2"""

parsed_test_input = parse_input([i.strip() for i in test_input.splitlines(keepends=False)])
print(parsed_test_input[-1])
parsed_input = parse_input(inputt)


@pytest.mark.parametrize(
    "inputt, output",
    [(parsed_test_input, 5)],
)
def test_part1(inputt, output):
    rv = part1(inputt)
    assert rv == output


# @pytest.mark.parametrize(
#     "inputt, output",
#     [(["forward 5", "down 5", "forward 8", "up 3", "down 8", "forward 2"], 900), (inputt, 1408487760)],
# )
# def test_part2(inputt, output):
#     rv = part2(inputt)
#     assert rv == output
