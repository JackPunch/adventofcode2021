from typing import List, Tuple
from dataclasses import dataclass


@dataclass
class VentPath:
    start: Tuple[int, int]
    end: Tuple[int, int]

    def get_path(self, part2: bool = False):
        if self.start[0] == self.end[0] or self.start[1] == self.end[1]:  # Horizontal or vertical
            if self.start[0] - self.end[0] == 0:  # check vertical distance
                return self.horizontal_path()
            else:
                return self.vertical_path()

        elif part2:
            return self.diagonal_path()

        else:
            return None

    def horizontal_path(self):
        sort_vals = sorted([self.start, self.end])
        return [
            (sort_vals[0][0], sort_vals[0][1] + i)
            for i in range(abs((sort_vals[1][1] - sort_vals[0][1])) + 1)
        ]

    def vertical_path(self):
        sort_vals = sorted([self.start, self.end])
        return [
            (sort_vals[0][0] + i, sort_vals[0][1])
            for i in range(abs((sort_vals[1][0] - sort_vals[0][0])) + 1)
        ]

    def diagonal_path(self):
        """Needs to be sorted for each coord now"""
        sort_vals = sorted([self.start, self.end])
        diff = abs(self.start[0] - self.end[0])
        if sort_vals[0][1] < sort_vals[1][1]:
            return [(sort_vals[0][0] + i, sort_vals[0][1] + i) for i in range(diff + 1)]
        else:
            return [(sort_vals[0][0] + i, sort_vals[0][1] - i) for i in range(diff + 1)]


class Field:
    def __init__(self):
        self.rows: List[List[int]] = [[0]]

    def add_coords(self, coords: List[Tuple[int, int]]):
        # check dimensions
        for coord in coords:
            self.check_size(coord)  # Move out to an initial max size check
            self.rows[coord[0]][coord[1]] += 1

    def check_size(self, coord: Tuple[int, int]):
        if len(self.rows) - 1 < coord[0]:
            self.add_rows((coord[0] - (len(self.rows) - 1)))
        if len(self.rows[0]) - 1 < coord[1]:
            self.add_columns((coord[1] - (len(self.rows[0]) - 1)))

    def add_rows(self, n: int) -> None:
        """Add n rows to field"""
        try:
            current_columns = len(self.rows[0])
        except Exception:
            for i in range(n):
                self.rows.append([])
        for i in range(n):
            self.rows.append([0] * current_columns)

    def add_columns(self, n: int) -> None:
        """Add n columns to field"""
        for row in self.rows:
            row.extend([0] * n)

    def get_result(self):
        return sum([len(list(filter(lambda x: x > 1, row))) for row in self.rows])


def read_input(path: str, lines: bool = True):
    with open(path, "r") as f:
        input = f.readlines() if lines else f.read()
    return input


def parse_input(inputt: List[str]) -> List[VentPath]:
    vents = []
    for line in inputt:
        start, end = line.split(" -> ")
        start_ints = tuple(map(lambda x: int(x), start.split(",")))
        end_ints = tuple(map(lambda x: int(x), end.split(",")))
        vents.append(VentPath(start=start_ints, end=end_ints))

    return vents


def part1(vent_paths: List[VentPath]) -> int:
    field = Field()
    for vent_path in vent_paths:
        path = vent_path.get_path()
        if path is not None:
            field.add_coords(path)

    return field.get_result()


def part2(vent_paths: List[VentPath]) -> int:
    field = Field()
    for vent_path in vent_paths:
        path = vent_path.get_path(part2=True)
        if path is not None:
            field.add_coords(path)

    return field.get_result()


if __name__ == "__main__":

    inputt = read_input("Day5/input.txt")

    parsed = parse_input(inputt)

    print(part1(parsed))
    print(part2(parsed))
