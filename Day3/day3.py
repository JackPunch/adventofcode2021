import numpy as np
from typing import List
from collections import Counter


def get_input(path: str, lines: bool = True):
    with open(path, "r") as f:
        data = [line.strip() for line in f.readlines()] if lines else f.read().strip()
    return data


def part1(input: List[str]) -> int:
    np_input = np.array([list(map(lambda x: int(x), line)) for line in input])
    binary_number = ""
    for i in range(np_input.shape[1]):
        vert_slice = np_input[:, i]
        counter = Counter(vert_slice)
        most_common = counter.most_common(1)[0][0]
        binary_number += str(most_common)
    b = "".join("1" if x == "0" else "0" for x in binary_number)
    return int(binary_number, 2) * int(b, 2)


def part2(input: List[str]) -> int:
    np_input = np.array([list(map(lambda x: int(x), line)) for line in input])
    oxygen_list = np.copy(np_input)
    co2_list = np.copy(np_input)

    for i in range(np_input.shape[1]):
        if len(oxygen_list) == 1:
            break
        vert = oxygen_list[:, i]
        sum = np.sum(vert)
        if sum > (oxygen_list.shape[0] / 2):
            oxygen_list = np.array([x for x in oxygen_list if x[i] == 1])
        elif sum < (oxygen_list.shape[0] / 2):
            oxygen_list = np.array([x for x in oxygen_list if x[i] == 0])
        else:
            oxygen_list = np.array([x for x in oxygen_list if x[i] == 1])

    for i in range(np_input.shape[1]):
        if len(co2_list) == 1:
            break
        vert = co2_list[:, i]
        sum = np.sum(vert)
        if sum > (co2_list.shape[0] / 2):
            co2_list = np.array([x for x in co2_list if x[i] == 0])
        elif sum < (co2_list.shape[0] / 2):
            co2_list = np.array([x for x in co2_list if x[i] == 1])
        else:
            co2_list = np.array([x for x in co2_list if x[i] == 0])

    oxygen_value = int("".join([str(x) for x in oxygen_list[0]]), 2)
    co2_value = int("".join([str(x) for x in co2_list[0]]), 2)
    return oxygen_value * co2_value


if __name__ == "__main__":
    input = get_input("Day3/input.txt")
    print(part1(input))
    print(part2(input))
