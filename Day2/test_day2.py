import pytest
from day2 import part1, part2, read_input

input = read_input("Day2/input.txt")


@pytest.mark.parametrize(
    "input, output",
    [(["forward 5", "down 5", "forward 8", "up 3", "down 8", "forward 2"], 150), (input, 1690020)],
)
def test_part1(input, output):
    rv = part1(input)
    assert rv == output


@pytest.mark.parametrize(
    "input, output",
    [(["forward 5", "down 5", "forward 8", "up 3", "down 8", "forward 2"], 900), (input, 1408487760)],
)
def test_part2(input, output):
    rv = part2(input)
    assert rv == output
