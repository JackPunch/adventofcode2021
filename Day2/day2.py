def read_input(path: str, lines: bool = True):
    with open(path, "r") as f:
        input = f.readlines() if lines else f.read()
    return input


def part1(input: list[str]):
    parsed = [(split[0], int(split[1])) for split in [line.split() for line in input]]
    position = 0
    depth = 0
    for method, value in parsed:
        if method == "forward":
            position = position + value
        elif method == "down":
            depth = depth + value
        elif method == "up":
            depth = depth - value
    return position * depth


def part2(input: list[str]):
    parsed = [(split[0], int(split[1])) for split in [line.split() for line in input]]
    position = 0
    depth = 0
    aim = 0
    for method, value in parsed:
        if method == "forward":
            position = position + value
            depth = depth + (value * aim)
        elif method == "down":
            aim = aim + value
        elif method == "up":
            aim = aim - value
    return position * depth


if __name__ == "__main__":
    input = read_input("input.txt")
    print(part1(input))
    print(part2(input))
