from os import read
from typing import List


def read_input(path: str, lines: bool = True):
    with open(path, "r") as f:
        input = f.readlines() if lines else f.read()
    return input


def parse_input(inputt: List[str]) -> List[List[int]]:
    rv = []
    rv.extend([list(map(lambda x: int(x), line.strip())) for line in inputt])
    return rv


def part1(inputt: List[List[int]]) -> int:
    count = 0
    for i, line in enumerate(inputt):
        for j, number in enumerate(line):
            numbers_to_check = []
            try:
                if j - 1 < 0:
                    raise Exception
                numbers_to_check.append(line[j - 1])
            except Exception:
                pass
            try:
                numbers_to_check.append(line[j + 1])
            except Exception:
                pass
            try:
                if i - 1 < 0:
                    raise Exception
                numbers_to_check.append(inputt[i - 1][j])
            except Exception:
                pass
            try:
                numbers_to_check.append(inputt[i + 1][j])
            except Exception:
                pass
            if all([number < x for x in numbers_to_check]):
                count += number + 1
    return count


def part2(inputt: List[List[int]]) -> int:
    """
    Complete part2:

    Use the coordinates of low points from part1
    then just keep appending coordinates until a 9 is reached in every direction

    create graph and traverse all paths to a 9 then create a set of all nodes to reduce duplicates
    """
    pass


if __name__ == "__main__":
    inputt = read_input("Day9/input.txt")
    parsed = parse_input(inputt)
    print(part1(parsed))
